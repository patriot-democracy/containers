# PHP FPM Container

## PHP Versions

`Dockerfile` points to latest stable version.

If other PHP versions are required, a version specific docker container should 
be created.

EG: php7.2-fpm.Dockerfile

## INI's

A INI file can be created to override and replace the default file in a
container by making the filename the same.

By default we prefix our config files with `patriot-` to prevent file name 
clashes.
