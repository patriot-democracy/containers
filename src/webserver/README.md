# Local Web Server

As part of our local development support container we provide a simple and 
light-weight nginx image that only needs to serve up static files from a static 
site generator build.

See `patriot-cli` helper script.

## vhosts

This container image comes out the box with 2 vhosts to assist development.

- `patriot-democracy.test` - server last completed build
- `patriot-democracy-dev.test` - servers dynamic watch build

## Not For Production

This is a development only container and should not be used in production.
