#!/bin/bash

#===============================================================================
# Certbot Docker Deploy Helper
#===============================================================================

## To make it more obvious to a dev seeing output that something important
## happed we will try and use ANSI escape codes

## Ref: https://misc.flogisoft.com/bash/tip_colors_and_formatting

## Reset        0;        Bold          1;
## Black        0;30m     Dark Gray     1;30m
## Red          0;31m     Light Red     1;31m
## Green        0;32m     Light Green   1;32m
## Brown/Orange 0;33m     Yellow        1;33m
## Blue         0;34m     Light Blue    1;34m
## Purple       0;35m     Light Purple  1;35m
## Cyan         0;36m     Light Cyan    1;36m
## Light Gray   0;37m     White         1;37m

RED='\e[31m';
NC='\e[0m';           # No Colour / Reset
GREEN='\e[32m';
YELLOW='\e[1;33m';

#-------------------------------------------------------------------------------
# Get our vars
#-------------------------------------------------------------------------------

# Strip prefix and suffix quotes from environmental variables
HTTPS_DOMAINS="${HTTPS_DOMAINS%\"}";
HTTPS_DOMAINS="${HTTPS_DOMAINS#\"}";

# Shared volume location where certbot files are to be stored
CERTBOT_DATA_PATH=/etc/letsencrypt;

# Container shared cert volume
DOCKER_PRODUCTION_PATH=/etc/certificates

# Check and Handle our ACTION argument
if [ "$1" == "" ]; then
    echo -e "Action argument missing";
    exit 1;
fi

ACTION="$1";

# Date stamped of script execution
DATE_STAMP=$(date '+%Y-%m-%d_%H%M%S');

#-------------------------------------------------------------------------------
# Build Self Signed Local Certificates
#-------------------------------------------------------------------------------
# We try avoid the need for self signed certs, but sometimes it useful
# EG: If we need the a web-server start with HTTPS before certs have been issued
#-------------------------------------------------------------------------------

make_self_signed() {
    echo -e "${YELLOW}Makign self signed certificate${NC}";

    for DOMAIN in $HTTPS_DOMAINS
    do
        # Ensure our paths exist
        DOCKER_CERT_PATH="$CERTBOT_DATA_PATH/docker/keys/$DOMAIN";
        DOCKER_LIVE_PATH="$CERTBOT_DATA_PATH/docker/live/$DOMAIN";

        mkdir -p "$DOCKER_CERT_PATH"
        mkdir -p "$DOCKER_LIVE_PATH"

        # Chcek if this has already been done
        if [ -e "$DOCKER_CERT_PATH/privkey.pem" ] && [ -e  "$DOCKER_CERT_PATH/privkey.pem" ]
        then
            continue;
        fi

        # Create self signed certificates
        echo "${YELLOW}Generating local cert: ${GREEN}$DOMAIN${NC}";
        openssl req -x509 -nodes -newkey rsa:1024 -days 1 -keyout "$DOCKER_CERT_PATH/privkey.pem" -out "$DOCKER_CERT_PATH/fullchain.pem" -subj "/CN=localhost";

        # Create Symlinks for webserver to use
        ln -s "$DOCKER_CERT_PATH/privkey.pem" "$DOCKER_LIVE_PATH/privkey.pem";
        ln -s "$DOCKER_CERT_PATH/fullchain.pem" "$DOCKER_LIVE_PATH/fullchain.pem";
    done
}

#-------------------------------------------------------------------------------
# Issue Official Letencrypt Certificates using HTTP challenge
#-------------------------------------------------------------------------------

http_issue_certs() {
    echo -e "${YELLOW}Issuing certificate${NC}";

    # Ensure Variables
    STAGING_ARG="";
    DRYRUN_ARG="";
    EMAIL_ARG="";

    # Select appropriate email arguments
    EMAIL_ARG="--register-unsafely-without-email";
    EMAIL_VALUE="";

    if [ "$HTTPS_EMAIL" != "" ];
    then
        EMAIL_ARG="--email";
        EMAIL_VALUE="$HTTPS_EMAIL";
    fi

    # Enable staging mode if needed
    if [ "$HTTPS_STAGING" != "0" ];
    then
        echo -e "${YELLOW}CERTBOT Staging & Dry-run enabled:${NC} No real certificates will be generated";
        STAGING_ARG="--staging";
        DRYRUN_ARG="--dry-run";
    fi

    # Loop through domains and issue a certificate for each
    for DOMAIN in $HTTPS_DOMAINS
    do
        echo -e "${YELLOW}Issuing certificate:${NC} $DOMAIN";

        # Only run certbot if certificate does not already exist or we staging
        if [ -d "$CERTBOT_DATA_PATH/live/$DOMAIN" ] && [ "$HTTPS_STAGING" = "0" ];
        then
            echo -e "${YELLOW}Skipping domain:${NC} Cert already exists for ${YELLOW}'$DOMAIN'${NC}";
            continue;
        fi

        eval "certbot certonly --standalone --non-interactive --http-01-port=8080 --agree-tos $STAGING_ARG $DRYRUN_ARG $EMAIL_ARG $EMAIL_VALUE --rsa-key-size $HTTPS_RSA_KEY_SIZE -d $DOMAIN";
    done
}

#-------------------------------------------------------------------------------
# Renew Letencrypt Certificates
#-------------------------------------------------------------------------------

renew_certs() {
    echo -e "${YELLOW}Renew certificates${NC}";

    # Ensure Variables
    STAGING_ARG="";
    DRYRUN_ARG="";

    # Enable staging mode if needed
    if [ "$HTTPS_STAGING" != "0" ];
    then
        echo -e "${YELLOW}CERTBOT Staging enabled:${NC} No real certificates will be generated";
        STAGING_ARG="--staging";
        DRYRUN_ARG="--dry-run";
    fi

    # Check certificates and renew them
    eval "certbot renew $STAGING_ARG $DRYRUN_ARG";
}

#-------------------------------------------------------------------------------
# Issue Official Letencrypt Certificates using DNS challenge
#-------------------------------------------------------------------------------

dns_issue_certs() {
    echo -e "${YELLOW}Issuing certificate${NC}";

    # Ensure Variables
    STAGING_ARG="";
    DRYRUN_ARG="";
    EMAIL_ARG="";

    # Select appropriate email arguments
    EMAIL_ARG="--register-unsafely-without-email";
    EMAIL_VALUE="";

    if [ "$HTTPS_EMAIL" != "" ];
    then
        EMAIL_ARG="--email";
        EMAIL_VALUE="$HTTPS_EMAIL";
    fi

    # Enable staging mode if needed
    if [ "$HTTPS_STAGING" != "0" ];
    then
        echo -e "${YELLOW}CERTBOT Staging & Dry-run enabled:${NC} No real certificates will be generated";
        STAGING_ARG="--staging";
        DRYRUN_ARG="--dry-run";
    fi

    # Loop through domains and issue a certificate for each
    for DOMAIN in $HTTPS_DOMAINS
    do
        echo -e "${YELLOW}Issuing certificate:${NC} $DOMAIN";

        # Only run certbot if certificate path does not already exist or we staging
        if [ -d "$CERTBOT_DATA_PATH/live/$DOMAIN" ] && [ "$HTTPS_STAGING" = "0" ];
        then
            echo -e "${YELLOW}Skipping domain:${NC} Cert already exists for ${YELLOW}'$DOMAIN'${NC}";
            continue;
        fi

        eval "certbot certonly --non-interactive --dns-cloudflare --dns-cloudflare-credentials /root/.secrets/cloudflare.ini --preferred-challenges dns-01 --agree-tos $STAGING_ARG $DRYRUN_ARG $EMAIL_ARG $EMAIL_VALUE --rsa-key-size $HTTPS_RSA_KEY_SIZE -d $DOMAIN";
    done
}

#-------------------------------------------------------------------------------
# Check if domain has issued certificates and update links (web-servers only)
#-------------------------------------------------------------------------------

update_links() {
    echo -e "${YELLOW}Linking certs${NC}";

    for DOMAIN in $HTTPS_DOMAINS
    do
        # If Certs exist switch webserver symlinks
        DOMAIN_CERT_PATH="$CERTBOT_DATA_PATH/live/$DOMAIN";
        echo -e "${YELLOW}Checking cert path exists:${NC} $DOMAIN_CERT_PATH";

        # Check if certbot issued certificates for the  domain
        if [ ! -d "$DOMAIN_CERT_PATH" ]
        then
            echo -e "${RED}PATH NOT FOUND${NC}: ${YELLOW}$DOMAIN_CERT_PATH${NC}";
            continue;
        fi

        # Certificates exists
        echo -e "${GREEN}Cert path exists${NC}";
        echo -e "${YELLOW}Switching symbolic links:${NC} $DOMAIN";
        DOCKER_LIVE_PATH="$CERTBOT_DATA_PATH/docker/live/$DOMAIN";

        # Re-create symlinks to letsencrypt certs files
        rm "$DOCKER_LIVE_PATH/privkey.pem";
        ln -s "$DOMAIN_CERT_PATH/privkey.pem" "$DOCKER_LIVE_PATH/privkey.pem";

        rm "$DOCKER_LIVE_PATH/fullchain.pem";
        ln -s "$DOMAIN_CERT_PATH/fullchain.pem" "$DOCKER_LIVE_PATH/fullchain.pem";
    done
}

#-------------------------------------------------------------------------------
# Check if domain has issued certificates concatenate for HAProxy
#-------------------------------------------------------------------------------

concat_certs() {
    echo -e "${YELLOW}Concatenating certs${NC}";

    for DOMAIN in $HTTPS_DOMAINS
    do
        # If Certs exist switch webserver symlinks
        DOMAIN_CERT_PATH="$CERTBOT_DATA_PATH/live/$DOMAIN";
        echo -e "${YELLOW}Checking cert path exists:${NC} $DOMAIN_CERT_PATH";

        # Check if certbot issued certificates for the  domain
        if [ ! -d "$DOMAIN_CERT_PATH" ]
        then
            echo -e "${RED}PATH NOT FOUND${NC}: ${YELLOW}$DOMAIN_CERT_PATH${NC}";
            continue;
        fi

        # Certificates exists
        echo -e "${GREEN}Cert path exists${NC}";
        echo -e "${YELLOW}Concatenating certificates:${NC} $DOMAIN";

        # Combine certificate and private key for HAProxy
        PRODUCTION_PATH="$DOCKER_PRODUCTION_PATH/$DOMAIN.pem";
        cat "$DOMAIN_CERT_PATH/fullchain.pem" "$DOMAIN_CERT_PATH/privkey.pem" > "$PRODUCTION_PATH";
    done
}

#-------------------------------------------------------------------------------
# Single HAProxy that new certificates have been issued
#-------------------------------------------------------------------------------

update_proxy() {
    echo -e "${YELLOW}Updating proxy${NC}";

    for DOMAIN in $HTTPS_DOMAINS
    do
        echo -e "${YELLOW}Proxy domain:${NC} $DOMAIN";

        # HAProxy container volume path
        PROXY_PATH=/usr/local/etc/haproxy/certificates/$DOMAIN.pem;

        # Shared concatenated cert path
        DOMAIN_PRODUCTION_CERT="$DOCKER_PRODUCTION_PATH/$DOMAIN.pem";

        # Check if certificates for the  domain exists
        if [ ! -f "$DOMAIN_PRODUCTION_CERT" ]
        then
            echo -e "${RED}PATH NOT FOUND${NC}: ${YELLOW}$DOMAIN_PRODUCTION_CERT${NC}";
            echo -e "${YELLOW}Skipping${NC}";
            continue;
        fi

        echo -e "${YELLOW}Proxy cert path:${NC} $PROXY_PATH";
        echo -e "${YELLOW}Production cert path:${NC} $DOMAIN_PRODUCTION_CERT";

        # Start transaction
        # shellcheck disable=SC2086
        RESPONSE=$(echo -e "set ssl cert $PROXY_PATH <<\n$(cat $DOMAIN_PRODUCTION_CERT)\n" | socat tcp-connect:haproxy:9999 -)
        echo "$RESPONSE";

        # Commit transaction
        RESPONSE=$(echo -e "commit ssl cert $PROXY_PATH" | socat tcp-connect:haproxy:9999 -)
        echo "$RESPONSE";

        # Show certification info (not essential)
        RESPONSE=$(echo -e "show ssl cert $PROXY_PATH" | socat tcp-connect:haproxy:9999 -)
        echo "$RESPONSE";
    done
}

#-------------------------------------------------------------------------------
# Log boundary to see start of new execution
#-------------------------------------------------------------------------------

date_boundary() {
    printf "\n\n[%s]\n\n" "$DATE_STAMP";
}

#-------------------------------------------------------------------------------
# Call Action
#-------------------------------------------------------------------------------

case $ACTION in
    # Web-server support setup (experimental)
    make-self-www)
        date_boundary;
        make_self_signed;
        update_links;
        ;;

    # Web-server support setup (experimental)
    make-self-proxy)
        date_boundary;
        make_self_signed;
        concat_certs;
        ;;

    # HTTP challenge issue & renew certs
    issue-www)
        date_boundary;
        http_issue_certs;
        renew_certs;
        ;;

    # DNS challenge issue & renew certs
    issue-dns)
        date_boundary;
        dns_issue_certs;
        renew_certs;
        ;;

    # Web-server update symlinks
    update-www)
        date_boundary;
        update_links;
        ;;

    # HAProxy update cert
    update-proxy)
        date_boundary;
        concat_certs;
        update_proxy;
        ;;

    *)
        echo -e "${RED}Invalid action${NC}";
        exit 1;
        ;;
esac

#===============================================================================
