#!/bin/bash

# ==============================================================================
# Certbot Container Entry Point
# ==============================================================================
# We use Cron to safely handle looping / scheduling of renewals
# ------------------------------------------------------------------------------

LOG_LEVEL=0;
LOG_FILE="/var/log/cron.log";

# Run initial issuing of certs
echo "Running certbot";
certbot-helper issue-dns && certbot-helper update-proxy;

# Start cron daemon
echo "Starting cron";
crond -f -l "$LOG_LEVEL" -L "$LOG_FILE" -c /etc/crontabs;
