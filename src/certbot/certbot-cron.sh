#!/bin/bash

#===============================================================================
# Certbot period cron to renew certs
#===============================================================================

certbot-helper issue-dns || exit 1;
certbot-helper update-proxy;
