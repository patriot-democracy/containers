# Apache

A production Apache container optimised for performance and memory consumption.

The official httpd base images have been optimised and stripped down a lot over
the years, making Apache fare more performant out the box than it used to be.

We look to take this a step further by only loading the modules we will need.

As our needs change we may enable more modules over time.

## Requirements

- Static content
- PHP FPM
- URL Rewriting
- Basic Auth

## Modules

We list modules enabled or disabled from the default, followed by then the final 
result.

### Disabled Modules

- authz_host_module (shared)
- authz_groupfile_module (shared)
- reqtimeout_module (shared)
- filter_module (shared)
- status_module (shared)
- autoindex_module (shared)

### Enabled Modules

- proxy (shared)
- proxy_fcgi (shared)
- rewrite (shared)

### Loaded Modules

- core_module (static)
- so_module (static)
- http_module (static)
- mpm_event_module (shared)
- authn_file_module (shared)
- authn_core_module (shared)
- authz_user_module (shared)
- authz_core_module (shared)
- access_compat_module (shared)
- auth_basic_module (shared)
- mime_module (shared)
- log_config_module (shared)
- env_module (shared)
- headers_module (shared)
- setenvif_module (shared)
- version_module (shared)
- unixd_module (shared)
- dir_module (shared)
- alias_module (shared)
- rewrite_module (shared)
- proxy_module (shared)
- proxy_fcgi_module (shared)

## References

- https://httpd.apache.org/docs/2.4/mod/
- https://httpd.apache.org/docs/2.4/misc/perf-tuning.html
- https://haydenjames.io/strip-apache-improve-performance-memory-efficiency/
- https://www.keycdn.com/support/apache-modules
- https://cwiki.apache.org/confluence/display/httpd/PHP-FPM
- https://medium.com/@bagasdotme/serving-php-on-apache-httpd-with-php-fpm-224a98c7401e
- https://www.digitalocean.com/community/tutorials/how-to-configure-apache-http-with-mpm-event-and-php-fpm-on-ubuntu-18-04
- https://www.howtoforge.com/configuring_apache_for_maximum_performance
