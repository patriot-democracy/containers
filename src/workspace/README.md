# Dev Workspace

A local development workspace container built to support all dependencies of 
gridsome and node so the patriot democracy website can easily be built locally.

See `patriot-cli` for usage.
