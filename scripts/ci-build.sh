#!/bin/sh

# ------------------------------------------------------------------------------
# CI Pipeline Docker Build Helper
# ------------------------------------------------------------------------------
# Required ENV Variables:
# - CONTAINER_IMAGE
# - CONTAINER_TAG
# - CI_COMMIT_SHA (?)
# ------------------------------------------------------------------------------

# Pull existing without failing if does not exist
docker pull "$CONTAINER_IMAGE:$CONTAINER_TAG" || true;

# Built using pulled cache if exist
docker build --cache-from "$CONTAINER_IMAGE:$CONTAINER_TAG" --tag "$CONTAINER_IMAGE:$CONTAINER_TAG" ./src/"$CONTAINER_TAG" || exit 1;

# If on master deploy image artifact
if [ "$CI_COMMIT_REF_NAME" = 'master' ];
then
    # docker push "$CONTAINER_IMAGE:$CI_COMMIT_SHA"
    docker push "$CONTAINER_IMAGE:$CONTAINER_TAG" || exit 1;
fi
