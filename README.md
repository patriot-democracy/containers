# Patriot Democracy Docker Containers

All docker support, build & testing containers used for local development, CI pipelines and deployment.

To speed up our build pipelines and save usage time we create custom container images with everything we need baked in.

We build multiple different images based on purpose.

## Recommended documentation

* [Pushing and Pulling to and from Docker Hub](https://ropenscilabs.github.io/r-docker-tutorial/04-Dockerhub.html)
* [Pushing a Docker container image to Docker Hub](https://docs.docker.com/docker-hub/repos/#pushing-a-docker-container-image-to-docker-hub)

## Hosting

Nothing confidential is in our docker images so we can host the images publicly. For simplicity and tight integration we are using GitLab Docker Container Repository.

__Url:__ [](registry.gitlab.com/patriot-democracy/containers)

## Pipeline integration

Manually building of container images should be unnecessary as we are using GitLab build pipelines to build, update and push push our containers automatically.

## Basics of manually building and pushing

**From Registry**

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/patriot-democracy/containers:workspace --pull
```

**From Source**

```
docker build --tag=patriot-democracy/containers:workspace src/workspace/
docker login registry.gitlab.com
docker push egistry.gitlab.com/patriot-democracy/containers:workspace
```

## Tags

The tag name is used to manage multiple images with different configs and purposes.

**Current tags**

- `workspace`: Primary working and development environment for local development
- `support`: Used for running tests in CI pipelines
- `nginx`: Simple web-server to proxy requests to the workspace container in local development

## Manual Local usage

If you would like to use and run these Docker images locally you have two choices.

1. Pull from Gitlab registry **(recommended)** EG: `docker pull registry.gitlab.com/patriot-democracy/containers:workspace`
1. Build locally EG: `docker build --pull --tag=patriot-democracy/containers:workspace src/workspace/`

## Docker Compose usage

Recommended usage is through `docker-compose.yml` that will be found in the applicable repositories.
